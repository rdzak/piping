import asyncio
from mods.publish import publish
from mods.subscribe import subscribe
from mods.consume import consume
from mods.generate import generate
import time

async def main():
    data = generate()
    num_messages = 10
    publish(data, num_messages)

    start = time.perf_counter()
    await subscribe(consume)
    end = time.perf_counter()
    
    # this never happens before keyboard interrupt
    print(f'took {start - end} to consume AND send to API {num_messages} messages') 


if __name__ == '__main__':
    asyncio.run(main())