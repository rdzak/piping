import json
import AMPS
import sys
import os



def publish(data, times=1):
    uri_ = "tcp://127.0.0.1:9027/amps/json"

    client = AMPS.Client("QueuePublisher-%s"% os.getpid())

    try:
        client.connect(uri_)
        client.logon()
        for i in range (1, times + 1): 
            # todo: add unique master deal id
            client.publish("sample-queue", json.dumps(data))
            print(f'published {i} message')
        
    except AMPS.AMPSException as e:
        sys.stderr.write(str(e) + "\n")

    client.publish_flush(2000)
    client.close()

    print('done publishing')


