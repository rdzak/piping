import asyncio
import json
import time
import requests

# from mods.consume import gen_gen

def send(data):
    print('sending...')
    url = 'http://127.0.0.1:8888/'
    res = requests.post(url, json=data)
    # print(f'response length', len(res.text))
    # print(json.loads(res.text))

def send_gen(gen):
    print('sending generator!!!')
    url = 'http://127.0.0.1:8888/hw2'
    res = requests.post(url, data=gen())
    # print(f'response length', len(res.text))
    return res.text
    # print(len(res.text))

# not so sure if async and await needed here
def send_async_gen_NEW(gen):
    return asyncio.to_thread(send_gen, gen)

async def send_async_gen(gen):
    # print('sending ASYNC generator!!!')
    url = 'http://127.0.0.1:8888/hw2'
    res = requests.post(url, data=gen())
    # print(f'ASYNC response length', len(res.text))
    # print(len(res.text))


async def send_bunch_of_async_gens(gens_payloads):
    print('sending a BUNCH of ASYNC generators!!! but sequentially')
    for gp in gens_payloads:
        await send_async_gen(gp)

async def send_bunch_of_async_gens_in_parallel(gens_payloads):
    # print('sending a BUNCH of ASYNC generators!!! in parallel')
    results = await asyncio.gather(*[send_async_gen(gp) for gp in gens_payloads])
    print(20 * '-', 'results:')
    print(len(results))


async def next_classification(gen_payloads):
    for gp in gen_payloads:
        yield await send_async_gen(gp)

# this could be imported 
def gen_gen(data):
    def gen():
        yield data.encode()
    return gen


async def main(num):



    # a bunch
    gen_payloads = [gen_gen(f'generator message numer {n}') for n in range(num)]
    await send_bunch_of_async_gens_in_parallel(gen_payloads)

    # use asynciterable, in sequence
    # async for r in next_classification(gen_payloads):
    #     print(r)
        


    # single
    # gen = gen_gen('xxxxxx')
    # await send_async_gen(gen)

if __name__ == '__main__':
    start = time.perf_counter()
    n = 3000
    asyncio.run(main(n))
    end = time.perf_counter()
    print(f'took {start - end} to send to API {n} messages') 



    
    
    
    