
import time
import AMPS
import sys
import os
from AMPS import Command


async def subscribe(consumer):
    uri_ = "tcp://127.0.0.1:9027/amps/json"
    client = AMPS.Client("QueueConsumer-%s" % os.getpid() )
    
    try:
        client.connect(uri_)
        client.logon()

        # subscription_id = client.execute_async(
        #     Command("subscribe").set_topic("sample-queue"),
        #     consumer # RuntimeWarning: coroutine 'consume' was never awaited
        # )
        # keeps us in the loop :)
        # time.sleep(5)
        # print('done in sub, subscription_id:', 'subscription_id')
        
        for message in client.subscribe("sample-queue"):
            await consumer(message)
        print('done in sub NEVER HAPPENS - todo')
    
    except KeyboardInterrupt as k:
        print('ok, stopped the forever loop')





    except AMPS.AMPSException as e:
        sys.stderr.write(str(e) + "\n")


