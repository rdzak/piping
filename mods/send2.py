import aiohttp
import asyncio
import time

def gen_gen(data):
    def gen():
        yield data.encode()
    return gen



async def send(session, url, data):
    async with session.post(url, json=data) as response:
        return await response.json()

async def main(num):
    url = 'http://127.0.0.1:8888'
    # gen_payloads = [gen_gen(f'generator message numer {n}') for n in range(num)]

    payloads = ({"what": f'message numer {n}'} for n in range(num))
    
    

    async with aiohttp.ClientSession() as session:
        finished = await asyncio.gather(*[send(session, url, p) for p in payloads], return_exceptions=True)    

        # print(finished)





start = time.perf_counter()
n = 30000
asyncio.run(main(n))
end = time.perf_counter()
print(f'took {start - end} to send to API {n} messages') 