import time
from flask import Flask
import flask

app = Flask(__name__)

@app.post("/")
def hello_world():
    r = flask.request.get_json()
    print(len(r), type(r))
    return r 



@app.post("/hw2")
def hello_world2():
    r = flask.request.get_data()
    print(len(r), type(r))
    return r


if __name__ == "__main__":
    app.run(port=8888, debug=True)
