from mods.parse import parse

n = 0

async def consume(message):
    global n
    n += 1
    print('...consuming', n)
    print(type(message), message)
    data = message.get_data() # if I could get this to be a generator, or a stream...

    

    print('...consumed single message length:', len(data), type(data))
    message.ack() # make sure it works
    print('ack!')

    gen = gen_gen(data.encode()) # ok, faking a generator, that always has full message anyway
    await parse(gen)

def gen_gen(data):
    def gen():
        yield data
    return gen