class AnotherClass():
    def __init__(self, name='another'):
        self.name = name
        print('AnotherClass object created') # should not print when patched