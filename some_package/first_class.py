from proto.some_package.another_class import AnotherClass


class FirstClass():
    def __init__(self, name='default'):
        self.name = name
        self.object_instance = AnotherClass(name='third')
        print('FirstClass object created')