import unittest
from unittest.mock import MagicMock, patch

# import FirstClass
from proto.some_package.first_class import FirstClass

def my_func_under_test(client, topic, filtr):
    print('my_func_under_test:', client)
    return [x for x in client.sow(topic, filtr) if x.get_command() == 'sow']
        

class TestFirstClass(unittest.TestCase):
    # test another class instance was created  when first class was created with patch
    @patch('proto.some_package.first_class.AnotherClass')
    def test_first_class(self, mock_another_class_mock):
        
        FirstClass()
        mock_another_class_mock.assert_called_once_with(name='third')







    def test_iter(self):   
        

        class Client():

            class Sow():
                def __init__(self, type='sow'):
                    self.type = type
                def get_command(self):
                    return self.type

            def sow(self, topic, filter=''):
                for m in range(10):
                    if m == 0 or m == 9:
                        yield self.Sow('not-sow')
                    else:
                        yield self.Sow('sow')
            

        fake_client = MagicMock()
        fake_client.sow.side_effect = Client().sow

        r = my_func_under_test(client=fake_client, topic='my topic', filtr='my filter')
        fake_client.sow.assert_called_once_with('my topic', 'my filter')
        assert len(r) == 8



        

if __name__ == '__main__':
    unittest.main()